const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const pool = require("../../../postgres/db");
const jwtGenerator = require("../utilities/jwtGenerator");
// const validateInfo = require("../middleware/validateInfo");

//register route
router.post("/", async (req, res) => {
  try {
    const { name, username, password } = req.body;
    //Check to see if username is already taken
    const user = await pool.query("SELECT * FROM users WHERE username = $1", [
      username,
    ]);
    if (user.rows.length !== 0) {
      res.status(402).send("user already exists");
    }
    //Use bcrypt to hash a password and store it in the database
    const saltRounds = 10;
    const salt = await bcrypt.genSalt(saltRounds);
    const bcryptPassword = await bcrypt.hash(password, salt);

    console.log(`${username} has ${bcryptPassword}`);
    const newUser = await pool.query(
      "INSERT INTO users(name, username, password, avatar, subscribers) VALUES($1,$2,$3,'https://yt3.googleusercontent.com/ytc/AL5GRJVQocYM80GnFFHn2_6K8E-bUuTxkoG16s1mjA01Jg=s176-c-k-c0x00ffffff-no-rj',0) RETURNING *",
      [name, username, bcryptPassword]
    );
    //Create a JSON web token and send it to the client for AUTHORIZATION process
    const tokens = jwtGenerator(newUser.rows[0]);
    res.cookie("dis_token", tokens.accessToken, {
      httpOnly: true,
      sameSite: "none",
      secure: true,
    });
    res.status(201).json({
      user: newUser.rows[0],
      accessToken: tokens.accessToken,
    });
  } catch (error) {
    res.status(500).json({ error });
  }
});

module.exports = router;
