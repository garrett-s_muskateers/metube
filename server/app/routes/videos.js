const express = require("express");
const router = express.Router();
const videoController = require("../controllers/videoController");

router.get("/", videoController.getAllVideos);
router.get("/:id", videoController.getOneVideo);
router.get("/by/:id", videoController.getVideosBy);

module.exports = router;
