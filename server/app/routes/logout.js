const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();

router.delete("/", (req, res) => {
  // console.log(req.headers);
  try {
    const dis_cookie = req.cookies.dis_token;
    if (!dis_cookie) {
      return res.send({ msg: "theres no cookie" });
    }
    try {
      const payload = jwt.verify(dis_cookie, process.env.ACCESS_TOKEN_SECRET);
      console.log(payload);
      res.clearCookie("dis_token");
      res.status(200).send({ cookie: payload });
    } catch (error) {
      return res.status(403).send({ error });
    }
  } catch (error) {
    res.status(401).send({ error });
  }
});

module.exports = router;
