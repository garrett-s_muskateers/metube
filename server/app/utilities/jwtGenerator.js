const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();

function jwtGenerator({ user_id, username }) {
  const payload = { user_id, username };
  const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "1hr",
  });
  const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET);
  return { accessToken, refreshToken };
}

module.exports = jwtGenerator;
