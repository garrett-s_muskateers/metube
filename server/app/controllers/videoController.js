const pool = require("../../../postgres/db");

async function getAllVideos(req, res) {
  try {
    const { rows } = await pool.query(
      `SELECT * FROM videos JOIN users ON videos.user_id = users.user_id`
    );
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}

async function getOneVideo(req, res) {
  const { id } = req.params;
  try {
    const { rows } = await pool.query(
      `SELECT * FROM videos JOIN users ON videos.user_id = users.user_id WHERE videos.id = ${id};`
    );
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}
async function getVideosBy(req, res) {
  const { id } = req.params;
  try {
    const { rows } = await pool.query(
      `SELECT * FROM videos JOIN users ON videos.user_id = users.user_id WHERE users.user_id = ${id};`
    );
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}

module.exports = { getVideosBy, getAllVideos, getOneVideo };
