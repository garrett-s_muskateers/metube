const pool = require("../../../postgres/db");

async function getAllComments(req, res) {
  try {
    const { rows } = await pool.query(`SELECT * FROM comments`);
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}

async function getUserComments(req, res) {
  try {
    const { rows } = await pool.query(
      `SELECT * FROM comments JOIN users ON comments.user_id = users.user_id`
    );
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}

async function getOneComment(req, res) {
  const { id } = req.params;
  try {
    const { rows } = await pool.query(
      `SELECT * FROM comments WHERE id = ${id}`
    );
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}
async function postComment(req, res) {
  const { comment, likes, date_published, video_id, user_id } = req.body;
  try {
    const { rows } = await pool.query(
      "INSERT INTO comments (comment, likes, date_published, video_id, user_id) VALUES ($1, $2, $3, $4, $5) RETURNING *",
      [comment, likes, date_published, video_id, user_id]
    );
    res.json(rows[0]);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}

async function getCommentsOnVideo(req, res) {
  const { video_id } = req.params;
  try {
    const { rows } = await pool.query(
      "SELECT * FROM comments JOIN users ON comments.user_id = users.user_id WHERE video_id = $1 ORDER BY likes DESC",
      [video_id]
    );
    res.send(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}
module.exports = {
  getOneComment,
  getAllComments,
  getUserComments,
  postComment,
  getCommentsOnVideo,
};
