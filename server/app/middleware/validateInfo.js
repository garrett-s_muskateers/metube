function validateInfo(req, res, next) {
  // console.log("validating info...");
  const { name, username, password } = req.body;
  if (req.baseUrl === "/register") {
    // console.log("in register path");
    if (![name, username, password].every(Boolean)) {
      return res.json("Missing Information");
    }
  } else if (req.baseUrl === "/login") {
    // console.log("in login path");
    // console.log(username);
    // console.log(password);
    if (![username, password].every(Boolean)) {
      return res.json("Missing Information");
    }
  }
  // console.log("info validated!");
  next();
}

module.exports = validateInfo;
