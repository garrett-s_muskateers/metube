import "./SigninOverlay.css";
import Signin from "./Signin";
import Register from "./Register";
import { useContext, useRef, useEffect } from "react";
import SigninContext from "../../context/SigninContext";
function SigninOverlay() {
  const {
    signinOverlayIsVisible,
    setSigninOverlayIsVisible,
    registering,
    setRegistering,
  } = useContext(SigninContext);

  const ref = useRef(null);
  useEffect(() => {
    const handleClickOutside = (e) => {
      if (ref.current && !ref.current.contains(e.target)) {
        setRegistering(false);
        setSigninOverlayIsVisible(false);
      }
    };
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);

  return (
    <>
      <div
        className="overlay"
        style={{ display: signinOverlayIsVisible ? "block" : "none" }}
      >
        <div className="login-modal" ref={ref}>
          {registering ? <Register /> : <Signin />}
        </div>
      </div>
    </>
  );
}
export default SigninOverlay;
