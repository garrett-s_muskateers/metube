import { useContext } from "react";
import MetubeContext from "../../context/MetubeContext";
import SigninContext from "../../context/SigninContext";
import { API_URL } from "../../App";
function Signin() {
  const { setCurrentUser } = useContext(MetubeContext);
  const {
    signinOverlayIsVisible,
    setSigninOverlayIsVisible,
    registering,
    setRegistering,
  } = useContext(SigninContext);
  const handleSignIn = async (e) => {
    try {
      e.preventDefault();
      const username = e.target[0].value;
      const password = e.target[1].value;
      const response = await fetch(`${API_URL}/login`, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ username, password }),
      });
      const data = await response.json();
      setCurrentUser(data.user);
      setSigninOverlayIsVisible(!signinOverlayIsVisible);
      e.target.reset();
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <form className="signin-form" onSubmit={handleSignIn}>
        <div
          className="close-button"
          onClick={() => {
            setSigninOverlayIsVisible(!signinOverlayIsVisible);
          }}
        >
          X
        </div>
        <h2>Login Here!</h2>
        <label>Enter your Username</label>
        <input type="text" placeholder="username" required></input>
        <label>Enter your Password</label>
        <input type="password" placeholder="password" required></input>
        <div className="login-form-buttons-div">
          <button className="login-form-button">LOGIN</button>
          <button
            className="login-form-button"
            onClick={() => {
              setRegistering(!registering);
            }}
          >
            Create account
          </button>
        </div>
      </form>
    </>
  );
}

export default Signin;
