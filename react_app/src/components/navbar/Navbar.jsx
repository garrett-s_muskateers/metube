import { useEffect, useState } from "react";
import Banner from "./Banner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEllipsisV,
  faSearch,
  faMicrophone,
  faUser,
  faShield,
  faMoon,
  faLanguage,
  faShieldAlt,
  faGlobe,
  faKeyboard,
  faGear,
  faQuestionCircle,
  faMessage,
  faArrowAltCircleRight,
  faSun,
  faArrowLeft,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";
import Tippy from "@tippyjs/react";
import { useContext } from "react";
import MetubeContext from "../../context/MetubeContext";
import SigninContext from "../../context/SigninContext";
import SigninOverlay from "../Signin/SigninOverlay";
function Navbar() {
  const { currentUser, setCurrentUser, theme, setTheme } =
    useContext(MetubeContext);
  const [isDropdown, setIsDropdown] = useState(false);
  const [search, setSearch] = useState("");
  const [restricted, setRestricted] = useState({
    open: false,
    restrict: "Off",
  });
  const [location, setLocation] = useState({
    open: false,
    country: "United States",
  });
  const [isMobile, setIsMobile] = useState(false);
  const [isSearch, setIsSearch] = useState(false);
  const { signinOverlayIsVisible, setSigninOverlayIsVisible } =
    useContext(SigninContext);

  useEffect(() => {
    function handleResize() {
      if (window.innerWidth < 700) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
        setIsSearch(false);
      }
    }
    handleResize();
    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const onSearch = (e) => {
    setSearch(e.target.value);
  };

  const submitSearch = () => {
    setSearch("");
  };
  const toggleDropdown = () => {
    setIsDropdown(!isDropdown);
    setTheme((theme) => ({ ...theme, open: false }));
    setRestricted((restrict) => ({ ...restrict, open: false }));
    setLocation((loc) => ({ ...loc, open: false }));
  };

  const openTheme = () => {
    setIsDropdown(false);
    setTheme((theme) => ({ ...theme, open: true }));
  };

  const setDark = () => {
    setIsDropdown(true);
    setTheme((theme) => ({ open: false, apperance: "Dark Theme" }));
  };

  const setLight = () => {
    setIsDropdown(true);
    setTheme((theme) => ({ open: false, apperance: "Light Theme" }));
  };

  const openRestrict = () => {
    setIsDropdown(false);
    setRestricted((restrict) => ({ ...restrict, open: true }));
  };

  const toggleRestrict = () => {
    setRestricted((restri) => ({
      ...restri,
      restrict: restri.restrict === "Off" ? "On" : "Off",
    }));
  };

  const toggleSearch = () => {
    setIsSearch(!isSearch);
  };

  const showSigninOverlay = () => {
    setSigninOverlayIsVisible(!signinOverlayIsVisible);
  };

  const handleSignOut = async () => {
    setCurrentUser(null);
    try {
      const response = await fetch("http://localhost:3001/logout", {
        method: "DELETE",
        credentials: "include",
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };
  if (isSearch) {
    return (
      <div className="navbar">
        <SigninOverlay />
        <Tippy content="Back" arrow={false}>
          <div
            className="rounded-button"
            onClick={toggleSearch}
            style={{ marginLeft: "10px", marginTop: "7px" }}
          >
            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
          </div>
        </Tippy>
        <div className="search-container">
          <input
            type="text"
            className="search-bar"
            placeholder="Search"
            value={search}
            onChange={onSearch}
            // onSelect
            style={{ width: "300px" }}
          />
          <Tippy content="Search" arrow={false}>
            <FontAwesomeIcon
              className="search-bar-button"
              icon={faSearch}
              onClick={submitSearch}
            />
          </Tippy>
        </div>
        <Tippy content="Search with your voice" arrow={false}>
          <div
            className="rounded-button"
            style={{
              marginLeft: "7px",
              backgroundColor: "rgba(128, 128, 128, 0.5)",
              marginTop: "4px",
              "border-radius": "0px",
              marginRight: "20px",
            }}
          >
            <FontAwesomeIcon
              className="icon"
              icon={faMicrophone}
              style={{ marginTop: "3px", width: "40px" }}
            />
          </div>
        </Tippy>
      </div>
    );
  }
  return (
    <div className="navbar">
      <SigninOverlay />
      <Banner />

      {isMobile ? (
        <div className="small-container" style={{}}>
          <Tippy content="Search" arrow={false}>
            <div
              onClick={toggleSearch}
              className="rounded-button"
              style={{
                marginLeft: "40px",
                marginTop: "5px",
                marginRight: "3px",
                "padding-right": "10px",
                "padding-left": "10px",
                width: "100px",
              }}
            >
              <FontAwesomeIcon className="icon" icon={faSearch} />
            </div>
          </Tippy>
          <Tippy content="Search with your voice" arrow={false}>
            <div
              className="rounded-button"
              style={{
                marginLeft: "7px",
                marginTop: "4px",
                "border-radius": "0px",
                marginRight: "20px",
              }}
            >
              <FontAwesomeIcon
                className="icon"
                icon={faMicrophone}
                style={{ marginTop: "3px", width: "40px" }}
              />
            </div>
          </Tippy>
        </div>
      ) : (
        <div className="container-search">
          <div className="search-container">
            <input
              type="text"
              className="search-bar"
              placeholder="Search"
              value={search}
              onChange={onSearch}
              // onSelect
            />
            <Tippy content="Search" arrow={false}>
              <FontAwesomeIcon
                className="search-bar-button"
                icon={faSearch}
                onClick={submitSearch}
              />
            </Tippy>
          </div>
          <Tippy content="Search with your voice" arrow={false}>
            <div
              className="rounded-button"
              style={{
                marginLeft: "7px",
                backgroundColor: "rgba(128, 128, 128, 0.5)",
                marginTop: "4px",
              }}
            >
              <FontAwesomeIcon
                className="icon"
                icon={faMicrophone}
                style={{ marginTop: "2px", width: "40px" }}
              />
            </div>
          </Tippy>
        </div>
      )}

      <div className="search-container">
        <Tippy content="Settings" disabled={isDropdown} arrow={false}>
          <div
            className="rounded-button"
            onClick={toggleDropdown}
            style={{ marginTop: "8px" }}
          >
            <FontAwesomeIcon className="icon" icon={faEllipsisV} />
          </div>
        </Tippy>
        {isDropdown && (
          <div className="dropdown-menu">
            <ul style={{ marginTop: "0", marginBottom: "0" }}>
              <li>
                <FontAwesomeIcon className="icon" icon={faShield} />
                <a>Your data in G-Tube</a>
              </li>
              <hr className="dropdown-divider" />
              <li onClick={openTheme}>
                <FontAwesomeIcon
                  className="icon"
                  icon={theme.apperance === "Dark Theme" ? faMoon : faSun}
                />
                <a>Appearance: {theme.apperance}</a>
                <FontAwesomeIcon
                  className="icon"
                  icon={faArrowAltCircleRight}
                  style={{ marginLeft: "0px" }}
                />
              </li>
              <li>
                <FontAwesomeIcon className="icon" icon={faLanguage} />
                <a>Language: English</a>
                <FontAwesomeIcon
                  className="icon"
                  icon={faArrowAltCircleRight}
                  style={{ marginLeft: "52px" }}
                />
              </li>
              <li onClick={openRestrict}>
                <FontAwesomeIcon className="icon" icon={faShieldAlt} />
                <a>Restricted Mode: {restricted.restrict}</a>
                <FontAwesomeIcon
                  className="icon"
                  icon={faArrowAltCircleRight}
                  style={{ marginLeft: "35px" }}
                />
              </li>
              <li>
                <FontAwesomeIcon className="icon" icon={faGlobe} />
                <a>Location: {location.country}</a>
                <FontAwesomeIcon
                  className="icon"
                  icon={faArrowAltCircleRight}
                  style={{ marginLeft: "17px" }}
                />
              </li>
              <li>
                <FontAwesomeIcon className="icon" icon={faKeyboard} />
                <a>Keyboard shortcuts</a>
              </li>
              <hr className="dropdown-divider" />
              <li>
                <FontAwesomeIcon className="icon" icon={faGear} />
                <a>Settings</a>
              </li>
              <hr className="dropdown-divider" />
              <li>
                <FontAwesomeIcon className="icon" icon={faQuestionCircle} />
                <a>Help</a>
              </li>
              <li>
                <FontAwesomeIcon className="icon" icon={faMessage} />
                <a>Send feedback</a>
              </li>
            </ul>
          </div>
        )}
        {theme.open && (
          <div className="dropdown-menu">
            <div className="mic-container">
              <div
                onClick={toggleDropdown}
                className="rounded-button"
                style={{
                  marginTop: "0",
                  marginRight: "10px",
                  marginLeft: "7px",
                  marginBottom: "7px",
                }}
              >
                <FontAwesomeIcon className="icon" icon={faArrowLeft} />
              </div>
              <h5
                style={{
                  color: "var(--text)",
                  marginTop: "8px",
                  "margin-botton": "20px",
                }}
              >
                Appearance
              </h5>
            </div>
            <hr className="dropdown-divider" style={{ marginTop: "0" }} />
            <li onClick={setDark}>
              <FontAwesomeIcon
                className="icon"
                icon={theme.apperance === "Dark Theme" ? faCheck : faMoon}
              />
              <a>Dark Theme</a>
            </li>
            <li onClick={setLight}>
              <FontAwesomeIcon
                className="icon"
                icon={theme.apperance === "Light Theme" ? faCheck : faSun}
              />
              <a>Light Theme</a>
            </li>
          </div>
        )}
        {restricted.open && (
          <div className="dropdown-menu">
            <div className="mic-container">
              <div
                onClick={toggleDropdown}
                className="rounded-button"
                style={{
                  marginTop: "0",
                  marginRight: "10px",
                  marginLeft: "7px",
                  marginBottom: "7px",
                }}
              >
                <FontAwesomeIcon className="icon" icon={faArrowLeft} />
              </div>
              <h5
                style={{
                  color: "var(--text)",
                  marginTop: "8px",
                  "margin-botton": "20px",
                }}
              >
                Restricted Mode
              </h5>
            </div>
            <hr className="dropdown-divider" style={{ marginTop: "0" }} />
            <p
              style={{
                marginLeft: "15px",
                marginBottom: "0",
                color: "var(--text)",
              }}
            >
              This helps hid potentially mature videos.
            </p>
            <p
              style={{
                marginLeft: "15px",
                marginTop: "0",
                color: "var(--text)",
              }}
            >
              No filter is 100% accurate.
            </p>
            <p style={{ marginLeft: "15px", color: "var(--text)" }}>
              This setting only applies to this browser.
            </p>
            <div className="mic-container">
              <h3
                style={{
                  marginLeft: "15px",
                  color: "var(--text)",
                  "font-weight": "600",
                  "padding-top": "15px",
                }}
              >
                Activate Restricted Mode
              </h3>
              <input
                type="range"
                min="0"
                max="1"
                step="1"
                onChange={toggleRestrict}
                value={restricted.restrict === "Off" ? "0" : "1"}
                style={{
                  width: "30px",
                  height: "20px",
                  marginLeft: "10px",
                  marginTop: "22px",
                  marginRight: "8px",
                }}
              />
            </div>
          </div>
        )}
        {currentUser ? (
          <div
            className="signin-button oval-button"
            onClick={handleSignOut}
            style={{ marginTop: "12px" }}
          >
            <p style={{ color: "#1e90ff", marginTop: "15px" }}>Sign Out</p>
          </div>
        ) : (
          <div
            className="signin-button oval-button"
            style={{ marginTop: "12px" }}
            onClick={showSigninOverlay}
          >
            <FontAwesomeIcon
              icon={faUser}
              style={{ marginRight: "5px", color: "#1e90ff" }}
            />
            <p style={{ color: "#1e90ff", marginTop: "15px" }}>Sign In</p>
          </div>
        )}
      </div>
    </div>
  );
}

export default Navbar;
