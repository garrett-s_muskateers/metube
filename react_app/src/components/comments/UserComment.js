import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import Tippy from "@tippyjs/react";

const UserComment = ({ setNewest, setIsOpen, isOpen }) => {
  const [commentText, setCommentText] = useState(""); // Add state for the input value

  const handleLikedClick = () => {
    setNewest(false);
    setIsOpen(false);
  };

  const handleNewClick = () => {
    setNewest(true);
    setIsOpen(false);
  };

  const handleButtonClick = () => {
    setIsOpen(true);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const commentData = {
      comment: commentText,
      likes: 1000, // set default value for likes
      date_published: new Date().toISOString().slice(0, 10), // set current date
      video_id: 1, // replace with the correct video ID
      user_id: 6, // replace with the correct user ID
    };
    // Use fetch to post the comment data to the server
    try {
      const res = await fetch("/Comments", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(commentData),
      });
      const newComment = await res.json();
      console.log("New comment added:", newComment);
      setCommentText(""); // Clear the input after the comment is added
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <div className="comments-dropdown-container">
        <p className="comments-count">5 Comments</p>
        <div className="custom-dropdown-container">
          <Tippy content="Sort by" arrow={false}>
            <button
              className="custom-dropdown-button"
              onClick={handleButtonClick}
            >
              <FontAwesomeIcon icon={faBars} size="1x" />
              &nbsp;&nbsp;Sort by
            </button>
          </Tippy>
          {isOpen && (
            <ul className="custom-dropdown-menu">
              <li className="custom-dropdown-item" onClick={handleLikedClick}>
                Top comments
              </li>
              <li className="custom-dropdown-item" onClick={handleNewClick}>
                Newest first
              </li>
            </ul>
          )}
        </div>
      </div>
      <form className="addComment" onSubmit={handleSubmit}>
        <img
          src="https://www.jpl.nasa.gov/edu/images/news/astronaut.jpg"
          className="comment-avatar"
        />
        <div className="input-container">
          <input
            className="input"
            type="text"
            placeholder="Add a Comment"
            value={commentText}
            onChange={(e) => setCommentText(e.target.value)}
          />
        </div>
        <button type="submit">Submit</button>
      </form>
    </>
  );
};

export default UserComment;
