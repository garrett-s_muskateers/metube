import React from "react";
import Navbar from "../components/navbar/Navbar";
import Secondary from "../components/Secondary/Secondary";
import Video from "../components/video_component/Video";
import Details from "../components/details/Details";
import Comments from "../components/comments/Comments";
import { useContext } from "react";
import MetubeContext from "../context/MetubeContext";
import Queue from "../components/Secondary/QueueComp/Queue";
import { useLocation } from "react-router-dom";

function VideoPage() {
  const { theme } = useContext(MetubeContext);
  let { state } = useLocation();
  // console.log(state);
  return (
    <>
      <Navbar />
      <div className="primary-container">
        <Video video={state} />
        <Details videoID={state.id} />
        <Comments video_id={state.id} />
      </div>

      <Secondary />
    </>
  );
}

export default VideoPage;
