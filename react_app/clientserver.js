const PORT = 3004;

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('build'));

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.listen(PORT, () => {
    console.log(`Our app is running on port: ${PORT}`);
});
